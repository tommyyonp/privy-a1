/// <reference types="cypress" /> 
 
describe('Basic Tests', () => {
    beforeEach(() => {
        cy.viewport(1280, 720)
        cy.visit('https://codedamn.com')
    })
    it('We have correct page title', () => {
        cy.viewport(1280, 720)
        cy.visit('https://codedamn.com')
    
        //mocha
        cy.contains('Learn Programming').should('exist')
        cy.get('[data-testid="logo"]').click()
    })

    it('Login page looks good', () => {
        cy.contains('Sign in').click()
        cy.contains('Forgot your password?').should('exist')
        cy.contains('Create one').should('exist')
    })

    it('Login should display correct error', () => {
        cy.get('[data-testid=username]').type('admin')
        cy.get('[data-testid=password]').should('exist')
    })
})
